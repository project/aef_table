<?php

/**
 * Implementation of hook_field_info().
 */
function aef_table_field_info() {
  return array(
    'aef_table' => array(
      'label' => t('AEF table'),
      'description' => t('Table widget'),
    ),
  );
}

/**
 * Implementation of hook_field().
 */
function aef_table_field($op, &$node, $field, &$items, $teaser, $page) {
  switch ($op) {
    case 'validate':
      return $items;

    case 'sanitize':
  }
}


/**
 * Implementation of hook_content_is_empty().
 */
function aef_table_content_is_empty($item, $field) {
  if ((empty($item['fid']) || (string)$item['fid'] === '0')
    /** || data is empty */) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Implementation of hook_field_formatter_info().
 */
function aef_table_field_formatter_info() {
  return array(
    'default' => array(
      'label' => t('Default: HTML table'),
      'field types' => array('aef_table'),
      'multiple values' => CONTENT_HANDLE_CORE,
    ),
  );
}

/**
 * Theme function for 'default' text field formatter.
 */
function theme_aef_table_formatter_default($element) {

	// Inside a View this function may be called with null data.  In that case,
	// just return.
	if (empty($element['#item'])) {
		return '';
	}


  $html = theme('aef_table_default', $element['#item']['fid'], $element['#item']['data']);

  return $html;
}


/**
 * Implementation of hook_widget_info().
 *
 * Here we indicate that the content module will handle
 * the default value and multiple values for these widgets.
 *
 * Callbacks can be omitted if default handing is used.
 * They're included here just so this module can be used
 * as an example for custom modules that might do things
 * differently.
 */
function aef_table_widget_info() {
  return array(
    'aef_table_creator' => array(
      'label' => t('AEF table widget'),
      'field types' => array('aef_table'),
      'multiple values' => CONTENT_HANDLE_CORE,
      'callbacks' => array(
        'default value' => CONTENT_CALLBACK_DEFAULT,
      ),
    ),
  );
}

/**
 * Implementation of FAPI hook_elements().
 *
 * Any FAPI callbacks needed for individual widgets can be declared here,
 * and the element will be passed to those callbacks for processing.
 *
 * Drupal will automatically theme the element using a theme with
 * the same name as the hook_elements key.
 *
 * Autocomplete_path is not used by text_widget but other widgets can use it
 * (see nodereference and userreference).
 */
function aef_table_elements() {
  return array(
    'aef_table_creator' => array(
      '#input' => TRUE,
      '#columns' => array('nid', 'fid'), 
      '#delta' => 0,
      '#process' => array('aef_table_creator_process'),
      '#element_validate' => array('aef_table_creator_validate'),
      '#value_callback' => 'aef_table_widget_value',
      '#autocomplete_path' => FALSE,
    ),
  );
}

/**
 * Implementation of hook_widget_settings().
 */
function aef_table_widget_settings($op, $widget) {
  switch ($op) {
    case 'form':
      $form = array();

      $form['file_path'] = array(
        '#type' => 'textfield',
        '#title' => t('File path'),
        '#default_value' => is_string($widget['file_path']) ? $widget['file_path'] : '',
        '#description' => t('Optional subdirectory within the "%directory" directory where files will be stored. Do not include preceding or trailing slashes.', array('%directory' => variable_get('file_directory_path', 'files') . '/')),
        '#element_validate' => array('_aef_table_settings_file_path_validate'),
      );
      return $form;

    case 'validate':

      return;

    case 'save':
      return array('file_path');
  }
}

function _aef_table_settings_file_path_validate($element, &$form_state) {

}

/**
 * Implementation of hook_widget().
 *
 * Attach a single form element to the form. It will be built out and
 * validated in the callback(s) listed in hook_elements. We build it
 * out in the callbacks rather than here in hook_widget so it can be
 * plugged into any module that can provide it with valid
 * $field information.
 *
 * Content module will set the weight, field name and delta values
 * for each form element. This is a change from earlier CCK versions
 * where the widget managed its own multiple values.
 *
 * If there are multiple values for this field, the content module will
 * call this function as many times as needed.
 *
 * @param $form
 *   the entire form array, $form['#node'] holds node information
 * @param $form_state
 *   the form_state, $form_state['values'][$field['field_name']]
 *   holds the field's form values.
 * @param $field
 *   the field array
 * @param $items
 *   array of default values for this field
 * @param $delta
 *   the order of this item in the array of subelements (0, 1, 2, etc)
 *
 * @return
 *   the form item for a single element for this field
 */
function aef_table_widget(&$form, &$form_state, $field, $items, $delta = 0) {
  $element = array(
    '#type' => 'aef_table_creator',
    '#default_value' => isset($items[$delta]) ? $items[$delta] : '',
  );
  return $element;
}

/**
 * Process an individual element.
 *
 * Build the form element. When creating a form using FAPI #process,
 * note that $element['#value'] is already set.
 *
 * The $fields array is in $form['#field_info'][$element['#field_name']].
 */
function aef_table_creator_process($element, $edit, $form_state, $form) {

  $item = $element['#value'];
  $field = $form['#field_info'][$element['#field_name']];
  $field_key = $element['#columns'][0];
  $delta = $element['#delta'];
  $infos = array();

  //Fetch the infos we need
  $infos = $item;



  //Determine in which state we are
  //State = 1 : Initial state, only the file selection widget is here
  //State = 2 : File is stored
  $element_state = 1;
  if((!empty($infos['fid']) && (string)$infos['fid'] !== '0'))
  {
    $element_state = 2;
    $file_infos = aef_table_fid_load($infos['fid']);
  }


  // Set up the buttons first since we need to check if they were clicked.
  $element['aef_table_upload'] = array(
    '#name' => $element['#field_name'] .'_'. $element['#delta'] .'_upload',
    '#type' => 'submit',
    '#value' => t('Upload'),
    '#ahah' => array(
        "path" => "aef_table/ahah/" . $element['#type_name'] . '/' . $element['#field_name'] . '/' . $delta,
        "effect" => "fade",
        "wrapper" => $element['#id'] . '-wrapper', 
    ),
    '#field_name' => $element['#field_name'],
    '#post' => $element['#post'],
    '#attributes' => array('class' => 'aef_table_upload'),
  );
  $element['aef_table_remove'] = array(
    '#name' => $element['#field_name'] .'_'. $element['#delta'] .'_remove',
    '#type' => 'submit',
    '#value' => t('Remove'),
    '#ahah' => array(
        "path" => "aef_table/ahah/" . $element['#type_name'] . '/' . $element['#field_name'] . '/' . $delta,
        "effect" => "fade",
        "wrapper" => $element['#id'] . '-wrapper', 
    ),
    '#field_name' => $element['#field_name'],
    '#post' => $element['#post'],
    '#attributes' => array('class' => 'aef_table_remove')
  );


/**
  // Because the output of this field changes depending on the button clicked,
  // we need to ask FAPI immediately if the remove button was clicked.
  // It's not good that we call this private function, but
  // $form_state['clicked_button'] is only available after this #process
  // callback is finished.
  if (_form_button_was_clicked($element['viid_viewer']['remove'])) {
  }
*/


  //Put a div wrapper here
  $element['#prefix'] = '<div class="form-item">' .
   '<label>' . check_plain($element['#title']) . ':</label>' . 
   '<div id="' . $element['#id'] . '-wrapper" class="aef_table_wrapper" field_name="' . $element['#field_name'] . '" delta="' . $element['#delta'] . '">';
  $element['#suffix'] = '</div></div>';

  //Theme
  $element['#theme'] = 'aef_table_widget';

  //
  // At this points, state info is ready.
  //


  $element['upload'] = array(
    '#name' => 'files['. $element['#field_name'] .'_'. $element['#delta'] .']',
    '#type' => 'file',
    '#required' => $element['#required'],
    '#weight' => -1,
    '#description' => t('Upload a CSV file containing a table'),
  );
  if($element_state == 2)
  {
    $element['file_infos'] = array(
      '#value' => '<div>' . $file_infos['filename'] . '</div>',
      '#weight' => -1,
    );
  }



  $element['fid'] = array(
    '#type' => 'hidden',
    '#value' => $item['fid'],
  );



  $element['upload']['#access'] = ($element_state == 1);
  $element['aef_table_upload']['#access'] = ($element_state == 1);
  $element['aef_table_remove']['#access'] = ($element_state != 1);

  return $element;
}


/**
 * FAPI theme for an individual text elements.
 *
 * $element['#field_name'] contains the field name
 * $element['#delta]  is the position of this element in the group
 */
function theme_aef_table_creator($element) {
  return $element['#children'];
}


/**
 * The #value_callback for the aef_table_creator type element.
 */
function aef_table_widget_value($element, $edit = FALSE) {
  global $user;
  $upload_name = $element['#field_name'] .'_'. $element['#delta'];
  if(count($_FILES['files']['name']) > 0)
  {
    $upload_first_key = array_slice(array_keys($_FILES['files']['name']),0,1);
    $upload_first_key = $upload_first_key[0];
  }
  $field = content_fields($element['#field_name'], $element['#type_name']);



  if(is_array($edit))
    $item = array('fid' => $edit['fid']);



  if($edit && 
      (
        empty($_FILES['files']['name'][$upload_name]) == false
      )
    )
  {

    $dest = file_directory_path() . (($field['widget']['file_path'])?"/":"") . $field['widget']['file_path'];

    if (!file_check_directory($dest, FILE_CREATE_DIRECTORY)) 
    {
      watchdog('aef_table', 'The upload directory %directory for the file field %field (content type %type) could not be created or is not accessible. A newly uploaded file could not be saved in this directory as a consequence, and the upload was canceled.', array('%directory' => $dest, '%field' => $element['#field_name'], '%type' => $element['#type_name']));
      form_set_error($upload_name, t('The file could not be uploaded.'));
    }
    else
    {
      if (!$file = file_save_upload($upload_name, array(), $dest)) {
        watchdog('aef_table', 'The file upload failed. %upload', array('%upload' => $upload_name));
        form_set_error($upload_name, t('The file in the @field field was unable to be uploaded.', array('@field' => $element['#title'])));
      }
      else
      {
        //Ok, file uploaded!
        //Check if it is a CSV file
        if(substr($file->filename, -4) != ".csv")
        {
          form_set_error($upload_name, t('The file you uploaded is not a CSV file.'));
        }
        else
        {
          //Change the status of the upload to permanent
          $file->status = FILE_STATUS_PERMANENT;
          drupal_write_record('files', $file, 'fid');
          //Save the fid
          $item['fid'] = $file->fid;
        }
      }
    }
    
  }
  //"Remove" button clicked
  else if($edit && 
      (
        isset($element['#post'][$element['#field_name'] .'_'. $element['#delta'] .'_remove']) 
      )
    )
  {

      $item = array('fid' => 0);
    
  }


  if(!$edit){
    $item = $element['#default_value'];
  }

  return $item;
}


/**
 * Theme the widget
 */
function theme_aef_table_widget($element) {
  $html = "";

  $html .= drupal_render($element);

  return $html;
}

